/* ----------------------------------------
	This file contains all commands that
	require a higher-level of permission
	than usual, i.e. mods/developers
 ---------------------------------------- */

const logger  = require("../core/logger"),
	command = require("../core/command").Command,
	dio		= require("../core/dio"),
	helpers	= require("../core/helpers"),
	x 		= require("../core/vars");

const STRIKE_COUNT = 3;

// TODO - Might use this one in the future for "other" recorded bits?
// let cmdTest = new command("admin", "!log", "Test embed", function(data) {
// 	helpers.modEmbed(data, {
// 		admin: data.user,
// 		action: "log",
// 		icon: ":notepad_spiral:",
// 		user: "93816438145429504"
// 	});
// });

// cmdTest.permissions = [x.admin, x.mod];

let cmdSay = new command("admin", "!say", "Allows Masta to speak as Mastabot", function(data) {
	if (data.userID === x.stealth) dio.say(data.message.replace("!say ",""), data, x.chan);
});

let cmdPurge = new command("admin", "!purge", "Deletes the last *n* messages", function(data){
	if(data.args.length != 2){
		throw new Error("Wrong number of arguments given to purge command");
	}

	let del_count = parseInt(data.args[1]);

	if(del_count <= 0){
		throw new Error("Number of messages cannot be negative or 0");
	}

	/*
	Since we want to delete n messages before the one that triggered this command, we want to delete n+1
	and report back actual_del-1 since it will include the one included.
	*/
	let actual_del = data.messageManager.Delete(del_count + 1, data.channelID, data) - 1;
	dio.say("<@" + data.userID + "> is deleting the last " + actual_del + " messages", data);
});

cmdPurge.permissions = [x.admin, x.mod];

let cmdPurgeAlt = new command("admin", "!altpurge", "Deletes the last *n* messages", function(data){

	if (data.args.length != 2) throw new Error("Wrong number of arguments given to purge command");
	
	let count = parseInt(data.args[1]);
	if (count <= 0 || count > 99) throw new Error("Number of messages cannot be negative, 0, or greater than 99");

	let msgs;

	data.bot.getMessages({
		channelID: data.channelID,
		limit: count + 1
	}, (err, resp) => {
		if (err) {
			logger.log(`${err} | ${resp}`,"Error");
			return false;
		}

		msgs = resp.map(m => m.id);

		if (msgs.length < 1) throw new Error("Empty message array");

		msgs.forEach((m, i) => {
			setTimeout((i) => {
				data.bot.deleteMessage({
					channelID: data.channelID,
					messageID: m
				});

				if (i === msgs.length - 1) dio.say(`🕑 <@${data.userID}> has deleted the last ${count} messages`, data);
			}, i*300);
		});
	});
});

cmdPurgeAlt.permissions = [x.admin, x.mod];

let cmdNewBuild = new command("admin", "!build","Admin trigger to bring up verification link on new build, pinging LFG", function(data) {
	let v = [
		`:white_check_mark: A new build means it's time to verify, <@&${x.lfg}>! http://toothandtailgame.com/verify`,
		`:white_check_mark: DO IT. JUST, DO IT <@&${x.lfg}>: http://toothandtailgame.com/verify`,
		`:white_check_mark: <@&${x.lfg}>: Don't be a punk, verify that junk: http://toothandtailgame.com/verify`,
		`:white_check_mark: <@&${x.lfg}>: Verify the game or else you're lame. http://toothandtailgame.com/verify`
	];

	let n = Math.floor( Math.random()*v.length );
	dio.say(v[n], data);
});

cmdNewBuild.permissions = [x.admin, x.mod];

let cmdCheck = new command("admin","!check","Gets data about the user being checked",function(data) {
	let u = helpers.getUser(data.message),
		user = data.bot.servers[x.chan].members[u];

	if (!user) {
		dio.say(`🕑 Dont recognize member: \`${u}\``, data);
		return false;
	}

	dio.del(data.messageID, data);

	let uname = user.username,
		nick = (user.nick != undefined) ? `${user.nick}` : "<no nickname>";
	let d = new Date(user["joined_at"]),
		join = `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()} @ ${d.getHours()}:${d.getMinutes()}`;

	data.db.soldiers.child(u).once("value", function(snap) {
		let memsnap = snap.val(),
			votes = 0,
			antivotes = 0;
		// Log it
		logger.log(JSON.stringify(memsnap), "Info");
		// If users is in Firebase
		if(memsnap) {
			for ( let modvote in memsnap.vote ) {
				if (memsnap.vote[modvote].ticket) {
					votes++;
				} else { antivotes++; }
			}

			let info_embed = new helpers.Embed({
				title: `${uname} - #${user.id}`,
				color: (user.status === "online") ? 0x33ff33 : 0xff3333,
				description: `**Nickname**: ${nick} (${(memsnap.rank) ? memsnap.rank : "Unranked"})\n**Joined:** ${join}\n**Key Tickets**: **${votes - antivotes}** :tickets:  ${(memsnap.strikes) ? `\n**Strikes**: **${+memsnap.strikes}** ⚾` : ""} ${(memsnap.verified) ? `\n_Verified by ${helpers.getNickFromId(memsnap.verified, data.bot)}_ :envelope:`: ""}`
			});

			helpers.getAvatarURL(user, data.bot, function(res){
				info_embed.setThumbnail(res, 64, 64);
				dio.sendEmbed(info_embed, data);
			});
		} else {
			dio.say("🕑 Error checking user in Firebase.", data);
		}
	});
});

cmdCheck.permissions = [x.mod, x.admin];

let cmdStrike = new command("admin", "!strike", "Allows mods to cast a vote to ban someone.", (data) => {
	dio.del(data.messageID, data);
	let stupid = data.bot.servers[x.chan].members[ helpers.getUser(data.args[1]) ];

	// Can't strike mods, admins, or bots
	if (stupid.hasOwnProperty("roles") && (stupid.roles.includes(x.mod) || stupid.roles.includes(x.admin) || stupid.roles.includes(x.combot))) {
		dio.say("Whoa hey, I can't ban them. Talk to a dev.", data);
		return false;
	}

	// Make sure a strike is accompanied with a picture
	dio.say(`🕑 Remember to add screenshot in history of the offense the strike is for, <@${data.userID}>.`, data);

	data.userdata.getProp(stupid.id, "strikes").then( (strikes) => {
		//console.log(stupid.id, strikes, data.attachments);
		if (!strikes) {
			data.userdata.setProp({
				user: stupid.id,
				prop: {
					name: "strikes",
					data: 1
				}
			});

			helpers.modEmbed(data, {
				admin: data.user,
				action: "strike",
				icon: ":baseball:",
				user: stupid.id,
				strikeCount: 1
			});
		} else if (strikes+1 < STRIKE_COUNT) {
			data.userdata.setProp({
				user: stupid.id,
				prop: {
					name: "strikes",
					data: strikes+1
				}
			});

			helpers.modEmbed(data, {
				admin: data.user,
				action: "strike",
				icon: ":baseball:",
				user: stupid.id,
				strikeCount: 2
			});
		} else if (strikes+1 >= STRIKE_COUNT) {
			data.userdata.setProp({
				user: stupid.id,
				prop: {
					name: "strikes",
					data: strikes+1
				}
			});

			// Goodness gracious this is scary xD
			data.bot.ban({
				serverID: x.chan,
				userID: stupid.id
			}, (err)=> {
				if (err) {
					logger.log(`User ban failed. ${err}`,"Error");
					return false;
				} else {
					helpers.modEmbed(data, {
						admin: data.user,
						action: "strike",
						icon: ":door:",
						user: stupid.id,
						strikeCount: 3
					});
				}
			});
		}
	});
});

cmdStrike.permissions = [x.mod, x.admin];

let cmdMute = new command("admin", "!mute", "Mutes a user for X minutes", function(data){
	dio.del(data.messageID,data);

	if(data.args.length != 3){
		dio.say("🕑 Wrong number of arguments. Syntax is `!mute user_id time_in_minutes`", data);
		return;
	}

	let targetID = helpers.getUser(data.args[1]),
		time = parseInt(data.args[2]);

	if(!targetID){
		dio.say("🕑 The provided ID is somehow bad", data);
		return;
	}

	if(targetID == data.userID){
		dio.say("This command is for muting others. For muting yourself, I can recommend duck tape", data);
		return;
	}

	let targetRoles = helpers.getUserRoles(data.bot, targetID, data.serverID);

	if(targetRoles.indexOf(helpers.vars.admin) != -1){
		dio.say("Developers cannot be muted. _Especially_ not Masta...", data);
		return;
	}

	if(targetRoles.indexOf(helpers.vars.muted) != -1){
		dio.say(`<@${targetID}> is already muted`, data);
		return;
	}

	if(Number.isNaN(time)){
		dio.say(`${time} is not a number`, data);
		return;
	}

	if(time < 1 || time > helpers.vars.consts.MAX_TIMEOUT){
		dio.say(`🕑 Invalid time period: must be at least 1 minute and less than ${helpers.vars.consts.MAX_TIMEOUT} minutes`, data);
		return;
	}

	// Perform the mute
	helpers.muteID({
		uid: targetID,
		auth: data.userID,
		data: data,
		time: time
	});
});

cmdMute.permissions = [x.mod, x.admin];

let cmdRed = new command("admin", ["!codered","!gg","!defcon1"], "Silences channel incase of emergencies", function(data) {
	data.bot.editChannelPermissions({
		roleID: x.chan,
		channelID: data.channelID,
		deny: [11]
	}, (err, resp) => {
		if (err) {
			logger.log(`${err} | ${resp}`, "Error");
		} else {
			let v = [
				":rotating_light: **HOLY CRAP. Emergency mode activated.** All standard users have been muted.",
				":rotating_light: **OH SNAP. Emergency mode activated.** All standard users have been muted.",
				":rotating_light: **AY MI MADRE. Emergency mode activated.** All standard users have been muted.",
				":rotating_light: **LORD HAVE MERCY. Emergency mode activated.** All standard users have been muted."
			];

			let n = Math.floor( Math.random()*v.length );
			dio.say(v[n], data);
		}
	});
});

cmdRed.permissions = [x.mod, x.admin];

let cmdGreen = new command("admin", ["!codegreen","!ez","!defcon5"], "Silences community incase of emergencies", function(data) {
	data.bot.editChannelPermissions({
		roleID: x.chan,
		channelID: data.channelID,
		allow: [11]
	}, (err, resp) => {
		if (err) {
			logger.log(`${err} | ${resp}`, "Error");
		} else {
			let v = [
				":balloon: **Emergency mode disactivated.** You may now return to your regularly scheduled programs.",
				":sunglasses: **Emergency mode disactivated.** Be cool, fam.",
				":logo_tnt: **Emergency mode disactivated.** Now go play some TnT or somethin'...",
				":sweat_smile: **Emergency mode disactivated.** Phew! Crisis averted."
			];

			let n = Math.floor( Math.random()*v.length );
			dio.say(v[n], data);
		}
	});
});

cmdGreen.permissions = [x.mod, x.admin];

let cmdClearBugs = new command("admin", "!clearbugs", "Clears all the Firebase bugs recorded", async function(data) {
	await data.db.bugs.set({});
	dio.say("Nuked bug list.", data, data.userID);
});

cmdClearBugs.permissions = [x.admin];


let cmdRename = new command("admin", "!rename", "Renames a user", function(data) {
	const targetID = helpers.getUser(data.args[1]),
		oldNick = helpers.getNickFromId(targetID, data.bot);

	data.bot.editNickname({
		serverID: x.chan,
		userID: targetID,
		nick: data.args.slice(2).join(" ")
	}, (err, resp) => {
		if (err) {
			logger.log(`${err} | ${resp}`,"Error");
			return false;
		}

		dio.del(data.messageID, data);
		helpers.modEmbed(data, {
			admin: data.user,
			action: "rename",
			icon: ":crayon:",
			user: targetID,
			prevName: oldNick
		});
	});
});

cmdRename.permissions = [x.admin, x.mod];

let cmdWarn = new command("admin", "!warn", "Warn a user through the bot", function(data) {
	// Check for at least one argument
	if (!data.args[1]) {
		dio.say("🕑 I need at least a channel name to warn!", data);
		return false;
	}

	// Get the ID's out
	const channelID = helpers.getUser(data.args[1]),
		userID = (data.args[2]) ? helpers.getUser(data.args[2]) : null;

	// Create the embed
	let embed = new helpers.Embed({
		author: {
			name: "Issuing Warning"
		},
		color: 0xFFDC00,
		description: `<#${channelID}> ${(userID) ? "| <@"+userID+">" : ""} is going to receive a warning. **What for?**

:speak_no_evil: Inappropriate Language
:zipper_mouth: Spamming
:pill: Inappropriate Behavior`,
	});

	// Add additional fields
	embed.fields = [
		{
			name: "Channel ID:",
			value: channelID,
			inline: true
		}
	];

	// Add user field if we defined one
	if (userID) embed.fields.push({
		name: "User ID:",
		value: userID,
		inline: true
	});

	dio.sendEmbed(embed, data, x.history);
});

cmdWarn.permissions = [x.mod, x.admin];

module.exports.commands = [
	cmdSay, cmdNewBuild, cmdPurge, cmdPurgeAlt,
	cmdCheck, cmdStrike, cmdMute, 
	cmdRed, cmdGreen, cmdWarn,
	cmdClearBugs, cmdRename
];

