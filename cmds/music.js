let dio = require("../core/dio");
let ytdl = require("ytdl-core");
let snekfetch = require("snekfetch");
let tokens = require("../core/tokens");
let command = require("../core/command").Command;
let logger = require("../core/logger");
let x = require("../core/vars");

let youtubeToken = tokens.FBKEY2();
let queue = {
	items: [],
	currentSong: {},
	data: null,
	voiceChannel: null
};
let ratelimited = {};

async function play(data) {
	if (!allowedInChannel(data)) return false;
	let video;
	if (!data.args.slice(1).length) {
		return dio.say("Syntax should be `!play <song name>`", data);
	}
	if (
		!data.bot.servers[data.bot.channels[data.channelID].guild_id].members[
			data.userID
		].voice_channel_id
	)
		return dio.say("Join the voice channel silly!", data);
	if (queue.items.length >= 30) {
		return dio.say("Try again in a bit!", data);
	}
	if (
		ratelimited[data.userID] &&
		ratelimited[data.userID] > new Date().getTime()
	) {
		logger.log(
			`User ${data.userID} is ratelimited. They tried to run '${data.args}'`,
			logger.INFO
		);
		dio.del(data.event.d.id, data);
		return dio.say(
			`🕑 Gotta go fast fast, huh? Not now. Try in \`${Math.round(
				(ratelimited[data.userID] - new Date().getTime()) / 1000
			)}\` seconds`,
			data
		);
	}
	ratelimited[data.userID] = new Date().getTime() + 15000; // 15 second ratelimit
	try {
		video = await youtubeSearch(data.args.slice(1).join(" "));
	} catch (e) {
		dio.say("No videos found!", data);
		return;
	}
	logger.log(
		`Use ${data.userID} searched for ${data.args
			.slice(1)
			.join(" ")} and found ${video.url}`,
		logger.INFO
	);
	queue.items.push(video);

	// If it's the first item in the queue, we start the queue
	if (!queue.voiceChannel) {
		runSetup(data);
	} else {
		dio.say(`Added ${video.title} by ${video.channel} to the queue`, data);
	}
}

async function runSetup(data) {
	let voiceChannel =
		data.bot.servers[data.bot.channels[data.channelID].guild_id].members[
			data.userID
		].voice_channel_id;
	if (!voiceChannel)
		return dio.say("You need to join the voice channel first!", data);
	queue.data = data;
	queue.voiceChannel = voiceChannel;

	data.bot.joinVoiceChannel(voiceChannel, async function(err) {
		if (err) {
			return dio.say(`An error occured: \`${err.message}\``, data);
		}
		data.bot.getAudioContext(voiceChannel, async function(err2, stream) {
			if (err2) return dio.say(`An error occured: \`${err2.message}\``, data);

			queue.stream = stream;
			runQueue();
		});
	});
}

async function runQueue() {
	queue.skipped = false;
	if (queue.items.length == 0) {
		dio.say("The queue is empty :wave:", queue.data);
		let channelID =
			queue.data.bot.servers[
				queue.data.bot.channels[queue.data.channelID].guild_id
			].members[queue.data.bot.id].voice_channel_id;
		queue.data.bot.leaveVoiceChannel(channelID);
		queue.voiceChannel = null;
		return;
	}
	let newVideo = queue.items.shift();
	queue.currentSong = newVideo;
	dio.say(`Now playing ${newVideo.title} by ${newVideo.channel}`, queue.data);
	playVideo(newVideo);
}

async function playVideo(video) {
	queue.ytStream = ytdl(video.url);
	queue.stream.removeAllListeners("error");
	queue.ytStream.on("error", function(err) {
		logger.log(err, logger.ERROR);
		dio.say("Oh snap! An error occured playing that video!", queue.data);
		runQueue();
	});
	queue.stream.on("error", function(err) {
		logger.log(err, logger.WARN);
	});
	queue.ytStream.pipe(queue.stream, { end: false });
	queue.stream.once("done", async function() {
		if (queue.skipped) return;
		setTimeout(async function() {
			runQueue();
		}, 500);
	});
}

// When the entire bot breaks, call this :schatz:
async function stop(data) {
	if (!allowedInChannel(data)) return false;
	if (!queue.voiceChannel || !queue.stream)
		return dio.say("I'm not playing anything right now!", data);
	queue.stream.stop();
	queue = {
		items: [],
		currentSong: {},
		data: queue.data,
		voiceChannel: null
	};
}

let SKIP_LOCK = false;
async function skip(data) {
	if (SKIP_LOCK) return dio.say("No!", data);
	SKIP_LOCK = true;
	if (!allowedInChannel(data)) return false;
	if (!queue.voiceChannel)
		return dio.say("I'm not playing anything right now!", data);
	dio.say("Skipping!", data);
	queue.ytStream.destroy();
	queue.stream.stop();
	queue.skipped = true;
	// Believe it or not, this IS needed
	setTimeout(function() {
		data.bot.getAudioContext(queue.voiceChannel, async function(err, stream) {
			queue.stream = stream;
			runQueue();
			SKIP_LOCK = false;
		});
	}, 500);
}

async function current(data) {
	if (!allowedInChannel(data)) return false;
	if (!queue.voiceChannel)
		return dio.say("I'm not playing anything right now!", data);
	dio.sendEmbed(
		{
			title: queue.currentSong.title,
			author: {
				name: queue.currentSong.channel
			},
			thumbnail: {
				url: queue.currentSong.thumbnail
			},
			url: queue.currentSong.url
		},
		data
	);
}

async function youtubeSearch(query) {
	// We have 10 as maxresults here for a !choose command coming soon™
	let res = await snekfetch
		.get("https://www.googleapis.com/youtube/v3/search")
		.query({
			q: query,
			maxResults: 10,
			part: "snippet",
			type: "video",
			key: youtubeToken,
			safeSearch: "strict" // Better safe than sorry!
		});
	if (!res.body.items) throw "No videos found";
	let video = res.body.items[0];
	return {
		url: "https://youtube.com/watch?v=" + video.id.videoId,
		title: video.snippet.title,
		description: video.snippet.description.substring(0, 1500),
		thumbnail: video.snippet.thumbnails.default.url,
		channel: video.snippet.channelTitle
	};
}

// Clean up the ratelimit bucket every 5 minutes so we don't eat up memory
setInterval(() => {
	for (const id in ratelimited) {
		if (ratelimited[id] < new Date().getTime()) delete ratelimited[id];
	}
}, 5 * 60 * 1000);

async function queuecmd(data) {
	if (!allowedInChannel(data)) return false;
	if (!queue.voiceChannel)
		return dio.say("I'm not playing anything right now!", data);

	dio.sendEmbed(
		{
			title: `🎶 Current Queue (${queue.items.length + 1} items)`,
			color: 1540797,
			fields: [queue.currentSong]
				.concat(queue.items)
				.slice(0, 7)
				.map((item, i) => {
					return i < 6
						? {
							name: `${i == 0 ? "▶ " : ""}${item.title} - ${item.channel}`,
							value: item.url
						}
						: {
							name: `And ${queue.items.length - 5} other${
								queue.items.length > 6 ? "s" : ""
							}`,
							value: "Use `!fullqueue` to recieve the full list!"
						};
				})
		},
		data
	);
}

async function fullQueue(data) {
	if (!queue.voiceChannel && allowedInChannel(data))
		return dio.say("I'm not playing anything right now!", data);
	dio.say(
		`${
			queue.items.length > 10 ? "First 10 items of queue" : "Queue"
		}: (Add more with \`!play\`)
0. ${queue.currentSong.title} by ${queue.currentSong.channel} <${
	queue.currentSong.url
}>
${queue.items
		.slice(0, 10)
		.map(function(item, i) {
			return `${i + 1}. ${item.title} by ${item.channel} <${item.url}>`;
		})
		.join("\n")}`,
		data,
		data.userID
	);
}

let allowedChannels = [x.playground, x.testing];

function allowedInChannel(data) {
	let allowed = allowedChannels.includes(data.channelID);
	if (!allowed) {
		dio.say(`🕑 You can only run this in <#${x.playground}>!`, data);
	}
	return allowed;
}

module.exports.commands = [
	new command("community", "!play", "Plays music", play),
	new command("community", "!skip", "Skips the current song", skip),
	new command("community", "!stop", "Stops currently playing music", stop),
	new command("community", "!queue", "Shows the queue", queuecmd),
	new command(
		"community",
		"!current",
		"Shows the currently playing music",
		current
	),
	new command(
		"community",
		"!fullqueue",
		"Shows the full queue, not just part",
		fullQueue
	)
];
