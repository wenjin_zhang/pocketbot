/* ----------------------------------------
	This file controls all the automated
	tournament related commands
 ---------------------------------------- */

const logger  = require("../core/logger"),
	command = require("../core/command").Command,
	x = require("../core/vars"),
	dio = require("../core/dio"),
	helpers	= require("../core/helpers"),
	TOKEN 	= require("../core/tokens"),
	stripIndents = require("common-tags").stripIndents,
	zip = require("jszip"),
	http = require("https"),
	rq = require("request-promise-native"),
	Convert = require("xml-js"),
	challonge = require("challonge-node-ng");

const client = challonge.withAPIKey(TOKEN.CHALLONGE);

let currentTourney = null,
	tNum = null,
	tPlayers = {},
	tRound = null,
	tCount = 0,
	tourneyChan = ( helpers.isDebug() ) ? x.testing : x.pbcup;

// In case of reset/crash mid tourney, look for any open PB tournaments and set all the data from there
async function resumeTourney() {
	try {
		let tournies = await client.tournaments.index({ "subdomain": "pocketbotcup" }),
			openT = tournies.filter( t => t.state !== "complete");

		if (openT[0]) {
			// Set ID and count
			currentTourney = openT[0].id;
			tNum = tournies.length;

			// Let's get back the participant/round data
			let tData = await getTourneyData();

			tData.participants.forEach( obj => tPlayers[obj.participant.misc] = obj.participant.id);
			tCount = Object.keys(tPlayers).length;

			let earlyMatch = tData.matches.map( obj => obj.match )
					.filter( match => match.state === "open"),
				completeMatches = tData.matches.map( obj => obj.match )
					.filter( match => match.state === "complete");

			tRound = (earlyMatch[0]) ? earlyMatch[0].round : 1; // If we got back an open match, first should contain earliest round

			if (tRound === 3) {
				tRound = (earlyMatch[earlyMatch.length-1].round === 0) ? tRound : 4; // Unless of course Round 0 (which is the bronze match), is hiding at the end
			}

			if (tData.matches.length === completeMatches.length) tRound = 4; // If ALL matches are done, set to final round

			logger.log(`Found existing tournament: ${currentTourney} | Now resuming from Round ${tRound} with ${tCount} players.`, "OK");
		}
	} catch(e) {
		logger.log(e, "Error");
	}
	
}

resumeTourney();

// This will create the tournament in Challonge
async function makeTourney(data) {
	let cups = await client.tournaments.index({ "subdomain": "pocketbotcup" }),
		n = cups.length+1;

	tNum = n;

	const tournament = await client.tournaments.create({
		"tournament": {
			"name": `Pocketbot Cup #${n}`,
			"url": `pocketbotcup_${n}`,
			"subdomain": "pocketbotcup",
			"description": "Welcome to the new and fully automated <strong>Pocketbot Cup</strong>! This is a weekly cup run by Pocketbot every Monday to let players enjoy a small dose of competition, while helping analyze replay data with the latest patch. If you have any questions or suggestions, talk to Mastastealth on the <a href='http://discord.gg/pockwatch'>PWG Discord</a>.",
			"hold_third_place_match": true,
			"accept_attachments": true,
			"signup_cap": 8,
			"start_at": new Date(Date.now() + (60 * 60 * 1000)), // Start in 1h
			"show_rounds": true,
			"game_id" : 54849,
			"game_name": "Tooth and Tail",
			"check_in_duration": 20 // minutes
		}
	});

	logger.log(`Created tournament: ${tournament.id}`,"OK");
	currentTourney = tournament.id;

	// PB announces it
	dio.say(`:trophy: A new Pocketbot Cup has begun! Follow it on Challonge here: http://pocketbotcup.challonge.com/pocketbotcup_${n} \n\n Sign up using \`!signup\` or if you already have a challonge account, use \`!signup challonge_username\`. \n\n There are 8 slots available (and 2 backup slots). Tournament starts in 1 hour, check-ins open 15 minutes prior to start.`, data, tourneyChan);
}

async function addPlayer(data, cid, tid = null, tRole = null) {
	const stamp = ( helpers.isDebug() ) ? `__${Date.now()}` : false,
		u = { 
			"name": `${data.user}${(stamp) ? stamp : ""}`, 
			"misc": `${data.userID}`
		};

	// Set the challonge ID if they pass in an account
	if (cid) {
		u["challonge_username"] = cid;
		delete u.name;
	}

	// If this is a custom tournament, require a Challonge account
	if (tid && !cid) {
		dio.say(`<@${data.userID}>, you're going to need a Challonge account registered to join a custom tournament! Please use \`!challonge username\` to register your account, then you can remove/readd your :thumbsup: to try again.`, data, x.tourney);
		return false;
	}

	const cTourneyLocal = (tid) ? tid : currentTourney;
	const tChan = (tid) ? x.tourney : tourneyChan;

	try {
		let player = await client.participants.create(cTourneyLocal, {participant: u});

		// Ignore the rest if custom tournament 
		if (tid) { 
			// Add custom role
			if (tRole) data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: tRole
			});

			dio.say(`<@${data.userID}> has entered the tournament! ${(cid) ? ":comet:" : ""}`, data, tChan); 
			return false; 
		} 

		tCount++; 
		tPlayers[`${data.userID}`] = player.id; // Store a local list of players 

		data.bot.addToRole({ 
			serverID: x.chan, 
			userID: data.userID, 
			roleID: x.competitor 
		}, function(err,resp) { 
			if (err) logger.log(`${err} | ${resp}`, "Error"); 
		}); 

		if (tCount >= 9) { 
			dio.say(`<@${data.userID}> is on standby! ${(cid) ? ":comet:" : ""}`, data, tChan); 
		} else { 
			dio.say(`<@${data.userID}> has entered the Cup! ${(cid) ? ":comet:" : ""}`, data, tChan); 
		} 

		// Check if we've hit our player limit 
		if (tCount === 10) { 
			setTimeout( ()=> { 
				dio.say(":trophy: We have now reached the maximum amount of participants! I will announce check-ins later on.", data, tChan); 
			}, 1000); 
		} 
	} catch(e) {
		logger.log(e, "Error");
		dio.say(`🕑 An error occurred adding participant to tournament ${cTourneyLocal}. :frowning: \`\`\`${e}\`\`\``, data, tChan);
	}
}

async function deletePlayer(data, cid, did, tid = null,) {
	const cTourney = (tid) ? tid : currentTourney,
		tChan = (tid) ? x.tourney : tourneyChan;

	try {
		
		await client.participants.delete(cTourney, cid);
		dio.say(`🕑 <@${did}> has been removed from the tournament. Hope you can play next time!`, data, tChan);
		if (tPlayers[cid]) delete tPlayers[cid];
	} catch(e) {
		dio.say("🕑 Removal unsuccessful, guess you're stuck playing. Forever.", data, tChan);
		logger.log(`Failed for user: ${cid}`, "Error");
		logger.log(e,"Error");
	}
}

// Process check-ins and commence tournament
async function startTourney(data, tid = null) {
	// If we got a manual ID, use that, otherwise default
	const tChan = (tid) ? x.tourney : tourneyChan;
	const cTourneyScoped = (tid) ? tid : currentTourney;

	// If a normal user called this without a custom tournament, fail
	const u = data.bot.servers[x.chan].members[data.userID],
		uRoles = (u && u.hasOwnProperty("roles")) ? u.roles : [];

	if (!tid && (!uRoles.includes(x.mod) && !uRoles.includes(x.admin) && !uRoles.includes(x.adminbot) ) ) {
		dio.say("🕑 Are you trying to start a Pocketbot Cup? Don't do that.", data, tChan);
		return false;
	}

	// Needs to be 5 or more players for PB Cups
	if ((!tid && tCount >= 5) || tid) {
		try {
			if (!tid) await client.tournaments.proc_checkin(cTourneyScoped); // Don't process checkins for custom tournies
			await client.participants.randomize(cTourneyScoped);

			await client.tournaments.start(cTourneyScoped);
			const cTourney = await getTourneyData(cTourneyScoped);

			// Create dictionary with "Challonge User ID : Discord User ID" pairs
			const cPlayers = {};
			cTourney.participants.forEach( (obj) => {
				cPlayers[obj.participant.id] = (obj.participant.misc) ? obj.participant.misc : 0;
			});

			// Find all round 1 matches and return vs. strings with <@user> pings
			const cMatches = cTourney.matches.map( obj => obj.match )
				.filter( match => match.state === "open" && match.round === 1)
				.map( match => {
					return `:regional_indicator_${match.identifier.toLowerCase()}: <@${cPlayers[match.player1_id]}> vs. <@${cPlayers[match.player2_id]}>`;
				});

			dio.say(stripIndents`${(!tid) ? "Processed all players who checked in. " : ""}**Let the games begin! :mega:**

			Round 1 Matches :crossed_swords::
			${cMatches.join("\n")}

			**Reminder**: You **will** need to submit replays to log your scores, so make sure to save 'em! :floppy_disk:${(!tid) ? "Matches are **best of 3**." : ""}`, data, tChan);

			tRound = 1; // Start round 1

			if (tid) {
				currentTourney = tid;
				return false; // No role cleanup on custom tournies
			}

			// Remove all the people with Competitor who AREN'T playing
			const cPlaying = cTourney.participants.map( obj => obj.match )
				.filter( player => player && player.seed > 8);

			Object.values(cPlaying).forEach( (p, i) => {
				setTimeout( () => {
					data.bot.removeFromRole({
						serverID: x.chan,
						userID: data.userID,
						roleID: x.competitor
					}, (err,resp) => {
						if (err) logger.log(`${err} | ${resp}`, "Error");
					});
				}, (i+1)*600);
			});

		} catch (err) {
			logger.log(err, "Error");
			dio.say("Couldn't start tournament, check console for errors.", data, tChan);
		}
	} else {
		try {
			await client.tournaments.destroy(cTourneyScoped);
			dio.say("Not enough participants entered the tournament this week, so it will be cancelled. :frowning: Gather some more folks for next week!", data, tourneyChan);
			resetTourneyVars(data);
		} catch (err) {
			logger.log(err, "Error");
			dio.say("Couldn't destroy tournament, check console for errors.", data, tourneyChan);
		}
	}
}

async function finishTourney(data, tid) {
	const tourney = (tid) ? tid : currentTourney,
		notPBCup = (Object.keys(tPlayers).length > 0) ? false : true,
		tChan = (notPBCup) ? x.tourney : tourneyChan;

	try {
		const cTourney = await client.tournaments.finalize(tourney, { "include_participants": 1 }),
			tName = (notPBCup) ? cTourney.name : "A Pocketbot Cup";

		// Announce winners
		let winners = {};
		cTourney.participants
			.filter(p => !p.participant["has_irrelevant_seed"])
			.map( (obj) => {
				//console.log(obj.participant); // Need console.log for the raw object

				// Earn WIP
				if ( helpers.isHeroku() ) {
					let wip = 5;

					if (obj.participant.final_rank === 1 ) { wip = 50; }
					else if (obj.participant.final_rank === 2) { wip = 30; }
					else if (obj.participant.final_rank === 3) { wip = 15; }
						
					data.userdata.transferCurrency(null, obj.participant.misc, wip, true).then( (res) => {
						logger.log(`Giving ${obj.participant.misc} ${wip} WIP`, "OK");
						if ( res.hasOwnProperty("err") ) logger.log(res.err, "Error");
					});
				}

				let notTop3 = (obj.participant.final_rank > 3) ? `-${obj.participant.seed}` : false;
				winners[`${obj.participant.final_rank}${(notTop3) ? notTop3 : ""}`] = (obj.participant.misc) ? obj.participant.misc : obj.participant.name;
			});

		logger.log(`Winners: ${winners["1"]}, ${winners["2"]}, ${winners["3"]}`, "Info");

		dio.say(stripIndents`The tournament has come to a close! :tada: Our winners are:

			:first_place: <@${winners["1"]}> +50 ${x.emojis.wip}
			:second_place: <@${winners["2"]}> +30 ${x.emojis.wip}
			:third_place: <@${winners["3"]}> +15 ${x.emojis.wip}

			**All** other participants will receive 5 ${x.emojis.wip} as well. Thanks for playing, see you next time!`, data, tChan);

		dio.say(stripIndents`${tName} has just finished, our winners are:

			:first_place: <@${winners["1"]}>
			:second_place: <@${winners["2"]}>
			:third_place: <@${winners["3"]}>

			${(notPBCup) ? "" : "If you wanna participate, keep an eye out for the signups every Monday at 2/6PM EST!"}`, data, x.memchan);

		logger.log("Finished tournament.", "OK");
		resetTourneyVars(data);
	} catch (err) {
		logger.log(err, "Error");
		dio.say("Couldn't end tournament, check console for errors.", data, tChan);
	}
}

// For check in OR out
function tourneyCheckIn(data, checkin=true) {
	// Get the player's challonge player ID
	const pid = tPlayers[`${data.userID}`];

	// Check for tourney
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	if (checkin) {
		client.participants.checkin(currentTourney, pid).then( () => {
			dio.say(`:white_check_mark:  <@${data.userID}>, you're checked in!`, data, tourneyChan);
		}).catch( e => {
			logger.log(e, "Error");
			dio.say("🕑 There was an error checking in. Check-in opens 15 minutes before the tournament starts.", data, tourneyChan);
		});
	} else {
		client.participants.checkout(currentTourney, pid).then( () => {
			data.bot.removeFromRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.competitor
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					return false;
				}

				dio.say(`🕑 <@${data.userID}>, you're checked out! Hopefully one of the backups will take your place or you'll end up ruining everything for everyone. :grin:`, data, tourneyChan);
			});
		}).catch( e => {
			logger.log(e, "Error");
			dio.say("🕑 There was an error checking in. Check-in opens 10 minutes before the tournament starts.", data, tourneyChan);
		});
	}
}

// Retrieves tourney data with participants and matches as well
function getTourneyData(id=currentTourney) {
	return client.tournaments.show(id, { "include_participants" : 1, "include_matches" : 1 });
}

async function checkRound(data) {
	// Check to see if previous round is over yet
	try {
		const t = await getTourneyData(),
			matches = t.matches.map( obj => obj.match ),
			roundList = Array.from( new Set( matches.map(m => m.round) ) ),
			notPBCup = (Object.keys(tPlayers).length > 0) ? false : true,
			tURL = (notPBCup) ? t.full_challonge_url : `http://pocketbotcup.challonge.com/pocketbotcup_${tNum}`,
			tChan = (notPBCup) ? x.tourney : tourneyChan;

		// Construct the roundMap
		let roundMap = [null];
		roundMap = roundMap.concat(roundList); //[null, 1, 2, ...]
		const bronze = (roundMap[roundMap.length - 1] === 0) ? roundMap.pop() : false; // If we have a round 0 (bronze) at the end, pop it out
		if (bronze === 0) roundMap.splice(roundMap.length - 1, 0, bronze); // and at it back as the 2nd to last round

		// Check how many open matches exist for the round
		const open = matches.filter( match => match.state === "open" && match.round == roundMap[tRound]);

		logger.log(`Round ${roundMap[tRound]} - Open matches left: ${open.length} | ${roundMap}`,"Info");

		if (open.length === 0 && tRound < roundMap.length-1) {
			// Start next round
			tRound++;

			// Make arrays of players and matches
			let cPlayers = {},
				bronzeRound = (roundMap.includes(0)) ? roundMap.length-2 : null,
				finalRound = roundMap.length-1;

			// For each participant, create Challoge ID : Discord ID dictionary entry
			t.participants.forEach( (obj) => {
				cPlayers[obj.participant.id] = obj.participant.misc;
			});

			logger.log(`Starting Round ${tRound} (${roundMap[tRound]} in roundMap)`,"OK");

			let cMatches = t.matches.map( obj => obj.match )
				.filter( match => match.round === roundMap[tRound]) // Get the ones for current round
				.map( match => {
					if (match.identifier !== "3P") { // For normal matches
						return `:regional_indicator_${match.identifier.toLowerCase()}: <@${cPlayers[match.player1_id]}> vs. <@${cPlayers[match.player2_id]}>`;
					} else { // For the bronze match
						return `:third_place: <@${cPlayers[match.player1_id]}> vs. <@${cPlayers[match.player2_id]}>`;
					}
				});

			if ((bronzeRound && tRound === roundMap.length-3) || (!bronzeRound && tRound === roundMap.length-2)) {
				dio.say(stripIndents`**Semi-finals have begun! :mega:**

				Semi-final Matches :crossed_swords::
				${cMatches.join("\n")}

				${tURL}`, data, tChan);
			} else if (bronzeRound && tRound === bronzeRound) {
				// Finished Finals, time for bronze match
				dio.say(stripIndents`**Our finalists are ready, but will now rest as we see who takes the bronze! :mega:**
				${cMatches.join("\n")}

				${tURL}`, data, tChan);
			} else if (tRound === finalRound) {
				// Finished Finals, time for bronze match
				dio.say(stripIndents`**:trophy: :mega: And now for the grand finals! :mega: :trophy:**
				${cMatches.join("\n")}

				${(notPBCup) ?  "" : "Reminder: Finals are **best of 5**!"}`, data, tChan);
			} else {
				dio.say(stripIndents`
				Round ${tRound} Matches :crossed_swords::
				${cMatches.join("\n")}

				${tURL}`, data, tChan);
			}
		} else if (tRound === roundMap.length-1) {
			// Finished
			finishTourney(data);
		} else {
			dio.say(`Currently playing Round ${roundMap[tRound]} - Open matches left: ${open.length}`, data);
		}
	} catch(e) {
		logger.log(e, "Error");
	}
}

async function updateScore(data, file) {
	// All right, everything looks legit enough. Let's assume we have a zip with
	// 3 replay files in it, did we have a winner?
	let score = (data.args[1] && data.args[1].length === 3) ? data.args[1].split("-") : false,
		replayCount = checkZip(data),
		PBCup = (Object.values(tPlayers).length > 0) ? true : false;

	if (replayCount < 3) {
		dio.say("This archive doesn't have the correct amount of replays.", data);
		return false;
	}

	if (!score) {
		dio.say("What was the score? Use the syntax `!score x-y`, no spaces between the number and dash.", data);
		return false;
	}

	// Check scores to be realistic
	if (PBCup) {
		if (parseInt(score[0]) > 3 || parseInt(score[1]) > 3) {
			dio.say("Those are some fishy scores. Double check they are correct!", data);
			return false;
		}

		if (parseInt(score[0]) < 3 && parseInt(score[1]) < 3 && tRound === 4) {
			dio.say("Final round is best of 5, something's not right here. :thinking:", data);
			return false;
		}
	}

	// Ok, we do have a winner, commence score upload stuff!
	let winnerID,
		winnerCID,
		cTourney = await getTourneyData(),
		matches = cTourney.matches.map( obj => obj.match ),
		roundList = Array.from( new Set( matches.map(m => m.round) ) );

	let roundMap = [null];
	roundMap = roundMap.concat(roundList); //[null, 1, 2, ...];

	const bronze = (roundMap[roundMap.length - 1] === 0) ? roundMap.pop() : false; // If we have a round 0 (bronze) at the end, pop it out
	if (bronze === 0) roundMap.splice(roundMap.length - 1, 0, bronze); // and add it back as the 2nd to last round;

	// Set the winner
	if (score[0] > score[1]) {
		winnerID = data.userID;

		// Get the winner's challonge ID
		if (PBCup) { // If PB Cup
			for (let p in tPlayers) {
				if (p === winnerID) winnerCID = tPlayers[p];
			}
		} else {
			// If it's a custom cup, we'll need to do a little more work
			const players = cTourney.participants.map( p => p.participant );
			players.forEach(p => {
				if (p.misc == winnerID) winnerCID = p.id;
			});
		}
		
	} else {
		// Welp, now I gotta find the opponent automatically.
		cTourney.matches.forEach( obj => {
			// Only check open matches
			if (obj.match.state === "open") {
				// See if person uploading is player1 or 2 of the match
				let players = [obj.match.player1_id, obj.match.player2_id];

				// For PB Cups, use tPlayer data
				if (PBCup) {
					if (tPlayers[data.userID] == players[0]) { // Uploader is player 1, so winner is player2
						for (let w in tPlayers) { if (tPlayers[w] === players[1]) winnerID = tPlayers[w]; }
						winnerCID = players[1];
					} else if (tPlayers[data.userID] == players[1]) { // Uploader is player 2, winner is player 1
						for (let w in tPlayers) { if (tPlayers[w] === players[0]) winnerID = tPlayers[w]; }
						winnerCID = players[0];
					}
				} else { // Otherwise, do more hunting
					let myCID;

					// First get my challonge ID
					const allPlayers = cTourney.participants.map( p => p.participant );
					allPlayers.forEach(p => { if (p.misc == data.userID) myCID = p.id; });

					// Now see if winner is player 1 or 2
					winnerCID = (myCID == players[0]) ? players[1] : players[0];
				}
			}
		});
	}

	if (winnerCID === null || winnerCID === undefined) {
		dio.say("Hmm, I couldn't find a proper Challonge ID to match your Discord ID. That's not good...", data);
		return false;
	}

	// Now let's look at the correct match and update it accordingly
	let match;

	matches.forEach( obj => {
		logger.log(`Analysing ${obj.id} | ${obj.round} for Round ${tRound}`,"Info");
		if (obj.state === "open" && obj.round == roundMap[tRound]) {
			logger.log(`Looking for ${winnerCID}...`, "Info");
			if (obj.player1_id == winnerCID || obj.player2_id == winnerCID)  match = obj;
		}
	});

	// Attach link to match
	try {
		logger.log(match.id, "Info");
		logger.log("Attaching match replays...", "Info");

		await client.matches.add_attach(currentTourney, match.id, { match_attachment: { url: file.url, description: file.filename } });

		// Format things correctly and update score
		logger.log("Formatting score for update", "Info");
		if (score[0] > score[1] && match.player1_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[0]}-${score[1]}`, "winner_id":winnerCID } });
		} else if (score[1] > score[0] && match.player1_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[1]}-${score[0]}`, "winner_id":winnerCID } });
		} else if (score[0] > score[1] && match.player2_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[1]}-${score[0]}`, "winner_id":winnerCID } });
		} else if (score[1] > score[0] && match.player2_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[0]}-${score[1]}`, "winner_id":winnerCID } });
		}

		// Confirm score submitted
		let tChan = (PBCup) ? tourneyChan : x.tourney;
		dio.say("Score updated. :tada: Please wait for round to finish before starting your next match. You will be notified when this happens.", data);
		dio.say(`<@${data.userID}> has updated their score. :tada:`, data, tChan); // TODO - Replace with an embed replay summary?

		checkRound(data);
	} catch(err) {
		logger.log(err, "Error");
		dio.say(`Dang it, something went wrong with the score submission. Try again? If you think you did everything correctly, talk to <@${x.stealth}>! :scream: \n \`\`\`Error: ${err}\`\`\``, data);
	}
}

function resetTourneyVars(data) {
	// Remove ALL competitors
	Object.keys(tPlayers).forEach( (p, i) => {
		logger.log(`Removing Competitor role from ${p}`, "OK");
		setTimeout( () => {
			data.bot.removeFromRole({
				serverID: x.chan,
				userID: p,
				roleID: x.competitor
			}, (err,resp) => {
				if (err) logger.log(`${err} | ${resp}`, "Error");
			});
		}, (i+1)*600);
	});

	// Now reset the vars
	tPlayers = {},
	tCount = 0,
	tRound = 1,
	currentTourney = null;
}

function checkZip(data, analyze=true, sendEmbed=false) {
	let dfile = data.attachments[0];

	let request = http.get(dfile.url, (response)=> {
		if (response.statusCode !== 200) {
			logger.log(`Response status was ${response.statusCode}`,"Error");
			dio.say("🕑 Error retrieving the file. :frowning:", data);
			return false;
		}

		let zdata = [];

		response.on("data", (chunk)=> {
			zdata.push(chunk);
		});

		response.on("end", function() {
			let buf = Buffer.concat(zdata);

			if (dfile.filename.endsWith(".xml") && sendEmbed) {
				analyzeReplays(data, buf, sendEmbed);
				return 0;
			}

			zip.loadAsync(buf).then( (z) => {
				let zLen = 0;

				z.forEach( async (replay) => {
					zLen++;

					try {
						const xml = await z.file(replay).async("string");
						if (analyze) analyzeReplays(data, xml, sendEmbed);
					} catch (err) {
						logger.log(err,"Error");
					}
				});

				const replayCount = zLen;
				dio.del(data.messageID, data);
				return replayCount;
			}).catch( (e) => {
				logger.log(e,"Error");
			});
		});
	});

	// check for request error too
	request.on("error", function (err) {
		dio.say("🕑 Oops, I couldn't upload the replay.", data);
		logger.log(err.message, "Error");
		return false;
	});
}

async function analyzeReplays(d, xml, sendEmbed) {
	if ( helpers.isDebug() ) return false; // Let's not bother in debug mode?

	let json = await convertReplay(xml);

	try {
		const fid = await d.db.balance.child("replays_v2").push(json);
		const players = json.Players.Player;

		logger.log(`Saved replay data from ${players[0].Identity.attr.Name} vs ${players[1].Identity.attr.Name} to Firebase.`, "OK");

		if (sendEmbed) replayEmbed(d,json,fid);
	} catch(e) {
		dio.say("🕑 Error analyzing the replays.", d);
		logger.log(e, "Error");
	}
}

function replayEmbed(d,json,fid) {
	const players = json.Players.Player,
		p1 = players[0],
		p2 = players[1], 
		p1Win = (p1.ArmyIndex.txt === json.Results.WinningTeam.txt) ? true : false,
		time = parseInt(json.MatchTime.txt);

	let embed = new helpers.Embed({
		title: `:crossed_swords: ${p1.Identity.attr.Name} vs ${p2.Identity.attr.Name}`,
		color: (p1Win) ? getFaction(p1.FactionId.txt).color : getFaction(p2.FactionId.txt).color,
		description: `The battle lasted ${Math.round((time/60) * 10) / 10}m, with a victory for **${(p1Win) ? p1.Identity.attr.Name : p2.Identity.attr.Name}**`,
	});

	embed.fields = [
		{
			name: `${getFaction(p1.FactionId.txt).emoji} ${p1.Identity.attr.Name}'s Deck`,
			value: `${getDeck(p1.Deck.Cards.Card)}`
		},
		{
			name: `${getFaction(p2.FactionId.txt).emoji} ${p2.Identity.attr.Name}'s Deck`,
			value: `${getDeck(p2.Deck.Cards.Card)}`
		},
		{
			name: "---",
			value: `[View on ReplayUltima](https://mastastealth.github.io/replay-ultima/#/${fid.key})`
		}
	];

	dio.sendEmbed(embed, d);
}

function getFaction(n) {
	let e, c;
	switch(n) {
	case "0":
		e = x.emojis.hopper;
		c = 0xD21E1E;
		break;
	case "1":
		e = x.emojis.bellafide;
		c = 0x148CFA;
		break;
	case "2":
		e = x.emojis.archi;
		c = 0xFFDC00;
		break;
	case "3":
		e = x.emojis.qm;
		c = 0x2ECC40;
		break;
	}
	return { emoji: e, color: c };
}

function getDeck(arr) {
	let deck = arr.map( (unit) => {
		let u = unit.attr.Data.replace("warren_","").replace("structure_","");
		return x.emojis[u];
	});

	return deck.join(" ");
}

function convertReplay(file) {
	return new Promise(function(resolve, reject) {
		try {
			const raw = JSON.parse(Convert.xml2json(file, {
				compact: true,
				spaces: 2,
				ignoreDeclaration: true,
				attributesKey: "attr",
				textKey: "txt",
			})).EventHistory;

			// Cleanup Large Input Junk we can't understand anyway
			delete raw.History;
			delete raw.attr;

			resolve(raw);
		} catch (e) {
			reject(e);
		}
	});
}

// =================== END OF FUNCTIONS =======================

// Admin/Bot Only Commands

let cmdCreate = new command("tourney", "!makecup", "Create a new Pocketbot Cup", (data) => {
	dio.del(data.messageID, data);
	makeTourney(data);
});

cmdCreate.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdStart = new command("tourney", "!startcup", "Starts Pocketbot Cup", (data)=> {
	dio.del(data.messageID, data);
	startTourney(data);
});

cmdStart.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdEnd = new command("tourney", "!endcup", "Ends Pocketbot Cup", (data)=> {
	dio.del(data.messageID, data);
	let tid = (data.args[1]) ? data.args[1] : false;
	finishTourney(data, tid);
});

cmdEnd.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdSetT = new command("tourney", "!settid", "Override currentTourney", (data)=> {
	dio.del(data.messageID, data);
	let tid = (data.args[1]) ? parseInt(data.args[1]) : false;
	currentTourney = tid;
	dio.say(`currentTourney set to \`${tid}\``, data);
});

cmdSetT.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdSetTR = new command("tourney", "!settr", "Override tRound", (data)=> {
	dio.del(data.messageID, data);
	let n = (data.args[1]) ? parseInt(data.args[1]) : false;
	tRound = n;
	dio.say(`tRound set to \`${n}\``, data);
});

cmdSetTR.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdCheckInRemind  = new command("tourney", "!checkinremind", "Lists participants/matches in tournament", async (data) => {
	dio.del(data.messageID, data);

	dio.say(`Will all <@&${x.competitor}>s please \`!checkin\` if you can play in today's tournament? If you cannot, go ahead and \`!checkout\` instead. Also note, anyone who \`!signup\`s from this point on will be automatically checked in.`, data, tourneyChan);
});

cmdCheckInRemind.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

// Admin/Mod Helper commands

let cmdList = new command("tourney", "!lscup", "Lists participants/matches in tournament", async (data) => {
	dio.del(data.messageID, data);

	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Attempts to get a list of all players and matches with relevant properties
	try {
		let t = await getTourneyData(),
			players = t.participants.map( (p)=> {
				return `${p.participant.name} | ${p.participant.id} - ${(p.participant.checked_in) ? "✓" : "⛔"}`;
			}),
			matches = t.matches.map( (m)=> {
				return `${m.match.identifier} - ${m.match.id} - R${m.match.round} | ${(m.match.state == "complete") ? "☑" : "☐"}`;
			});

		dio.say(`Tournament ID: ${currentTourney} \n The current competitors are: \`\`\`${players.join("\n")}\n---\n${matches.join("\n")}\`\`\``, data);
	} catch(e) {
		logger.log(e, "Error");
	}
});

cmdList.permissions = [x.admin, x.mod];

let cmdAddMatch = new command("tourney", "!addmatch", "Adds match to tournament", async (data) => {
	dio.del(data.messageID, data);

	try {
		let score = data.args[1], // Should be in "X-Y" format
			winner = (data.args[2]) ? helpers.getUser(data.args[2]) : null,
			winnerCID = null;

		// Check for winner in the tournament
		if ( tPlayers.hasOwnProperty(winner) ) {
			winnerCID = tPlayers[winner];
		} else {
			dio.say("🕑 This user isn't even part of the tournament.", data, tourneyChan);
			return false;
		}

		// Get tournament data
		let tData = await getTourneyData();

		// Find the open matches with the winner specified
		let open = tData.matches
			.map( obj => obj.match )
			.filter( match => match.state === "open" && (match.player1_id === winnerCID || match.player2_id === winnerCID));

		// The match ID for challonge, if something is found
		let mid = (open[0]) ? open[0].id : false;

		// Check if we got a match to update or note
		if (!mid) {
			dio.say("🕑 Could not find a match to update. :thinking:", data, tourneyChan);
			return false;
		}

		// Update the match
		let match = await client.matches.update(currentTourney, mid, {
			match: {
				"scores_csv" : score,
				"winner_id" : winnerCID
			}
		});

		dio.say(`Match ${match.id} updated.`, data);
		checkRound(data);
	} catch (err) {
		logger.log(err, "Error");
	}
});

cmdAddMatch.permissions = [x.admin, x.mod];

let cmdCheckRound = new command("tourney", "!round", "Force checks the current round", (data) => {
	dio.del(data.messageID, data);

	try {
		checkRound(data);
	} catch(e) {
		logger.log(e, "Error");
	}
});

cmdCheckRound.permissions = [x.admin, x.mod];

let cmdDQ = new command("tourney", "!dq", "Forcefully removes player from tournament", (data) => {
	dio.del(data.messageID, data);

	let player = helpers.getUser(data.args[1]);

	// Check for an actual tournament
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if you already signed up
	if (!tPlayers.hasOwnProperty(player)) {
		dio.say("🕑 This user isn't even part of the tournament.", data, tourneyChan);
		return false;
	}

	deletePlayer(data, tPlayers[player], player);
	checkRound(data);
});

cmdDQ.permissions = [x.admin, x.mod];

let cmdRando = new command("tourney", "!randomize", "Forcefully randomizes player seeds", async (data) => {
	dio.del(data.messageID, data);

	// Check for an actual tournament
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	await client.participants.randomize(currentTourney);

	dio.say("🕑 Randomized Player Seeds!", data);
});

cmdRando.permissions = [x.admin, x.mod];

// Non-admin commands

let cmdAdd = new command("tourney", ["!signup","!signin"], "Adds player to tournament", async (data) => {
	logger.log(`${data.userID} attempting signup.`);
	if (data.messageID) dio.del(data.messageID, data);

	const cid = await data.userdata.getProp(data.userID, "challonge"); // Challonge username (optional for PB Cup)
	const tid = (data.args[1]) ? data.args[1] : null;

	// Check for an actual tournament
	if (!currentTourney && !tid) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if you already signed up
	if (tPlayers.hasOwnProperty(data.userID) && helpers.isHeroku() && !tid ) {
		dio.say("🕑 You've already signed up. :tada:", data, tourneyChan);
		return false;
	}

	// Otherwise, if we're under 10 participants, add to tournament
	if ((tCount < 10 && !tid) || tid) {
		const tRole = (data.tRole) ? data.tRole : null;
		addPlayer(data, cid, tid, tRole);
	} else {
		dio.say("🕑 The tournament has reached the maximum number of entries. Hope to see you next week!", data, tourneyChan);
	}
});

let cmdDel = new command("tourney", ["!unsignup","!signdown","!signout"], "Removes player to tournament", async (data) => {
	logger.log(`${data.userID} attempting signout.`);
	if (data.messageID) dio.del(data.messageID, data);

	const tid = (data.args[1]) ? data.args[1] : null;

	// Check for an actual tournament
	if (!currentTourney && !tid) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if you already signed up
	if (!tPlayers.hasOwnProperty(data.userID) && helpers.isHeroku() && !tid) {
		dio.say("🕑 You're not even part of the tournament. :expressionless:", data, tourneyChan);
		return false;
	}

	let pid = (tPlayers.length > 0) ? tPlayers[data.userID] : await data.userdata.getProp(data.userID, "challonge");

	deletePlayer(data, pid, data.userID, tid);
	checkRound(data);
});

let cmdCheckIn = new command("tourney", "!checkin", "Checks In to tournament", (data) => {
	dio.del(data.messageID, data);
	tourneyCheckIn(data);
});

let cmdCheckOut = new command("tourney", "!checkout", "Checks Out of tournament", (data) => {
	dio.del(data.messageID, data);
	tourneyCheckIn(data, false);
});

let cmdUpdateScore = new command("tourney", "!score", "Updates score in tournament", (data) => {
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if this was done via DM or not
	if ( helpers.getDMChannel(data.bot, data.userID) ) {
		let dm = helpers.getDMChannel(data.bot, data.userID);

		if (data.channelID !== dm) {
			dio.say("🕑 Sorry, but could you DM me the score? That way we can avoid cluttering the channel. :blush:", data);
			return false;
		}

		// Check if we're looking for files from this person anyway
		if (helpers.isHeroku() && !tPlayers.hasOwnProperty(data.userID) ) {
			if (Object.values(tPlayers).length > 0) {
				dio.say("🕑 Uhm...you don't seem to be in the tournament, so I'm going to go ahead and ignore your file. :sweat_smile:", data, data.userID);
				return false;
			}
		}

		// Check if message is missing attachment
		if (data.attachments.length === 0) {
			dio.say("There was no file attached. Make sure to use this command in the comment portion of the **file upload dialog** when attaching the replay.", data, data.userID);
			return false;
		}

		// If someone in the tournament DOES upload a file
		let file = data.attachments[0];
		//console.log(file);

		// If file is not a zip
		if (!file.filename.endsWith(".zip")) {
			dio.say("Please attach a zip with ALL of your replays for your current set.", data, data.userID);
			logger.log(`${file.filename} is not a zip.`,"Error");
			return false;
		}

		// Check size to have a minimum, since it should be 3 replay files, but ignore in debug mode
		// if (helpers.isHeroku() && file.size <= 70000) {
		// 	dio.say("This is a suspiciously small zip for 3 replays. Are you sure you included all the files? If so, ping Masta or a mod for help.", data, data.userID);
		// 	return false;
		// }

		updateScore(data, file);
	} else {
		dio.del(data.messageID, data);
		dio.say("🕑 I have DM'd you the information you need to update your score. :thumbsup:", data);
		dio.say(stripIndents`Hey! To update your score, drag and drop a zip file containing the replays for your current set.

			When the file upload dialog pops up, enter in the command syntax: \`!score x-y \` where:

			\`x-y\` is the score, \`x\` is **your** score, \`y\` is your opponent's, e.g. 2-1 or 0-3.

			**NOTE:** If you make a mistake, contact <@${x.stealth}> or a mod.`, data, data.userID);
	}
});

cmdUpdateScore.permissions = [x.noob, x.member];

let cmdBracket = new command("tourney", "!bracket", "Link to the current brackets", async (data) => {
	dio.del(data.messageID, data);

	const notPBCup = (Object.keys(tPlayers).length > 0) ? false : true;
	let tURL; 

	if (notPBCup) {
		let t = await getTourneyData();
		tURL = t.full_challonge_url;
	} else {
		tURL = `http://pocketbotcup.challonge.com/pocketbotcup_${tNum}`;
	}

	dio.say(`:trophy: The current tournament bracket can be found at: ${tURL}`, data);
});

let cmdUp = new command("tourney", ["!addreplays","!replay"], "Manual replay upload processing", (data) => {
	checkZip(data, true, true);
});

cmdUp.permissions = [x.noob, x.member];

let cmdChallonge = new command("tourney", "!challonge", "Set your challonge username for PB Cups", async (data) => {
	const cid = data.args[1];
	dio.del(data.messageID, data);

	let cidFB = await data.userdata.setProp({
		user: data.userID,
		prop: {
			name: "challonge",
			data: cid
		}
	});

	if (cidFB) {
		dio.say(`All right, you have registered \`${cid}\` as your Challonge username. **Make sure this link points to your profile**: <http://challonge.com/users/${cid}> , otherwise you won't be able to properly sign up for tournaments!`, data, data.userID);
	} else {
		dio.say("🕑 Huh. Something went wrong here. Talk to Mastastealth about it.", data);
	}
});

let cmdAddTourney = new command("tourney", "!tourney", "Register a tournament with Pocketbot.", async (data) => {
	if (data.channelID !== x.tourney) {
		dio.say(`🕑 You can only host tournaments in the <#${x.tourney}> channel. :stuck_out_tongue_winking_eye: `, data);
		return false;
	}

	let url;

	try {
		// Automatically track tournament if Challonge link is included
		url = data.message.match(/(https?:\/\/)?(\S+.)?(challonge.com\/\S+)/gi);

		const html = (url) ? await rq( url[0] ) : false,
			tidReg = /tournaments\/(\d{5,})/gi,
			htmlParse = (html) ? tidReg.exec(html) : false,
			tid = (html && htmlParse) ? parseInt(htmlParse[1]) : false;

		logger.log(`URL: ${url} | TID: ${tid}`, "Info");

		const t = (tid) ? await getTourneyData(tid) : null;

		if (!url) {
			dio.say("🕑 Sorry, but I only custom tournaments using <https://challonge.com/>. Please create one and make sure to add a link to your post!", data, x.tourney);
			return false;
		}

		if (tid && t.id == tid) {
			if (t.tournament_type !== "single elimination") {
				dio.say("🕑 Sorry, but I only support single elimination tournaments at the moment. :frowning:", data, x.tourney);
				return false;
			}

			if (!t.accept_attachments) {
				dio.say("🕑 Please enable match attachments, or I won't be able to track rounds properly!", data, x.tourney);
				return false;
			}

			dio.say(`Your tournament ID is: ${tid}. When you've closed signups, you can **start** your tournament with \`!starttourney ${tid}\`. :thumbsup:`, data, data.userID);
		}

		// First get list of existing
		data.bot.getPinnedMessages({
			channelID: x.tourney
		}, (err, resp)=> {
			if (err) {
				logger.log(`${err} | ${resp}`, "Error");
				dio.say("🕑 Oops. Had trouble with something there. Try again later?", data);
				return false;
			}

			// Otherwise copy message, and delete it.
			dio.del(data.messageID, data);

			// Create a role
			data.bot.createRole(x.chan, (err, resp)=> {
				if (err) {
					logger.log(err, "Error");
					dio.say("🕑 Oops. Had trouble with something there. Try again later?", data);
					return false;
				}

				// Create an embed
				let embed = {
					author: {
						name: `🏆 Tournament has been created by: ${data.user}!`
					},
					color: 0xffbc00,
					description: data.message.replace("!tourney",""),
					footer: {
						text: "To signup for this tournament, give this message a 👍"
					},
					fields: [{ 
						name: "Hosted by:",
						value: `<@${data.userID}>`,
						inline: true
					},{
						name: "Competitors:",
						value: `<@&${resp.id}>`,
						inline: true
					},{
						name: "Tournament ID:",
						value: tid
					}]
				};

				dio.sendEmbed(embed, data);

				// Edit it
				data.bot.editRole({
					serverID: x.chan,
					roleID: resp.id,
					name: (tid && t) ? t.name : "Custom Competitor",
					color: 0xFF9D00,
					mentionable: true
				});
			});
		});
	} catch(e) {
		logger.log(e, "Error");
		dio.say(`🕑 Doesn't seem like <${url}> is a proper link.`, data);
	}
	
});

cmdAddTourney.permissions = [x.noob, x.member];

let cmdWinTourney = new command("tourney", "!endtourney", "Allow tournament hosters to give WIP prize to 3 winners.", (data) => {
	const tid = (data.args[1]) ? parseInt(data.args[1]) : null;

	if (!tid) {
		dio.say("🕑 I'll need a specific tournament ID to end.", data);
		return false;
	}

	// First get list of existing
	data.bot.getPinnedMessages({
		channelID: x.tourney
	}, (err, resp)=> {
		if (err) {
			logger.log(err, "Error");
			dio.say("🕑 Oops. Had trouble with something there. Try again later?", data);
			return false;
		}

		// Check for existing members who are running tourneys
		let currentHosts = resp.map((t) => {
			const host = (t.embeds[0].fields) ? helpers.getUser(t.embeds[0].fields[0].value) : t.author.id;
			const role = (t.embeds[0].fields) ? helpers.getUser(t.embeds[0].fields[1].value) : false;
			const id = (host && role) ? t.id : null;

			return { host: host, role: role, id: id };
		}).filter(tournies => tournies.host === data.userID);

		// If you aren't hosting a tourney, then deny!
		if (currentHosts.length > 0 && !currentHosts.some(t => t.host == data.userID && t.id)) { 
			dio.say("🕑 I didn't find you in the tourney list. :thinking: Too bad! Next time, use `!tourney` to add your tournament!", data);
			return false;
		}

		if (currentHosts.length === 0) {
			dio.say("🕑 I didn't find any tournaments to end.", data);
			return false;
		}

		// Otherwise delete and reward.
		dio.del(data.messageID, data);

		// Announce
		const winners = data.args.slice(2);

		if (winners.length > 0) {
			dio.say(stripIndents`:medal: A tournament has just finished, our winner(s):

			${winners.join("\n\n")}

			Congrats to all our winner(s) and participants! If you ever need to host a \`!tourney\`, just let me know!`, data, x.memchan);
		} else {
			dio.say(stripIndents`:medal: A tournament has just finished, congrats to all the <@&${currentHosts[0].role}>s and thanks for playing! If anyone ever need to host a \`!tourney\`, just let me know!`, data, x.memchan);
		}

		// Give earnings
		const competitors = Object.values(data.bot.servers[x.chan].members).filter(u => u.roles && u.roles.includes(currentHosts[0].role));

		competitors.forEach(player => {
			const wip = (winners.length > 0 && winners.includes(player.id)) ? 40 : competitors.length;

			data.userdata.transferCurrency(null, player.id, wip, true).then( (res) => {
				logger.log(`Giving ${player.id} ${wip} WIP`, "OK");
				if ( res.hasOwnProperty("err") ) logger.log(res.err, "Error");
			});
		});

		// Remove role from everyone (by deleting it)
		data.bot.deleteRole({
			serverID: x.chan,
			roleID: currentHosts[0].role
		});

		// Remove the pin
		dio.del(currentHosts[0].id, data, x.tourney);
	});
});

let cmdListReplays = new command("tourney", "!lsreplays", "Lists all the replay attachments from last tournament", async (data) => {
	dio.del(data.messageID, data);

	const tid = (data.args[1]) ? parseInt(data.args[1]) : null;

	// Attempts to get a list of all players and matches with relevant properties
	try {
		let tournies = (tid) ? await client.tournaments.index() : await client.tournaments.index({ "subdomain": "pocketbotcup" }),
			pocketTournies = tournies.filter( t => t.state == "complete"),
			lastTID = pocketTournies[0].id, // Is sorted latest to oldest?
			lastT = (tid) ? await getTourneyData(tid) : await getTourneyData(lastTID),
			matches = lastT.matches.map( (m)=> {
				return m.match.id;
			}),
			replays = matches.map( async m => {
				try {
					let a = (tid) ? await client.matches.index_attach(tid, m) : await client.matches.index_attach(lastTID, m);
					return (a[0] && a[0].hasOwnProperty("url")) ? `<${a[0].url}>` : "";
				} catch(e) {
					logger.log(e, "Error");
					return null;
				}
				
			});

		dio.say("🕑 I'll PM you the list of all the replays once as soon as I gather them.", data);

		let replaysResolved = await Promise.all(replays);

		dio.say(`Replays for the last tournament: \n${replaysResolved.join("\n")}`, data, data.userID);
	} catch(e) {
		dio.say("🕑 Couldn't get the list of replays. :thinking:", data);
		logger.log(e, "Error");
	}
});

let cmdListTournies = new command("tourney", "!lstournies", "Lists all the tournies under specific subdomain", async (data) => {
	dio.del(data.messageID, data);

	const sub = (data.args[1]) ? data.args[1] : null;

	if (!sub) {
		dio.say("🕑 I'll need a specific subdomain.", data);
		return false;
	}

	// Attempts to get a list of all players and matches with relevant properties
	try {
		dio.say("🕑 I'll PM you the list of all the tournaments under that subdomain as soon as I gather them.", data);

		const tournies = await client.tournaments.index({ "subdomain": sub }),
			tMap = tournies.map(t => `${t.id} | ${t.name}`);

		dio.say(`Tournaments for subdomain \`${sub}\`: \`\`\`\n${tMap.join("\n")}\`\`\``, data, data.userID);
	} catch(e) {
		dio.say("🕑 Couldn't get the list of tournaments. :thinking:", data);
		logger.log(e, "Error");
	}
});

cmdListTournies.permissions = [x.noob, x.member];

let cmdStartTourney = new command("tourney", "!starttourney", "Starts custom tournament", (data)=> {
	dio.del(data.messageID, data);

	const tid = (data.args[1]) ? parseInt(data.args[1]) : null;

	if (!tid) {
		dio.say("🕑 I'll need a specific tournament ID to start.", data);
		return false;
	}

	if (currentTourney) {
		dio.say("🕑 Another tournament seems to be in progress, I can only handle one at a time. :frowning:", data);
		return false;
	}

	startTourney(data, tid);
});

cmdStartTourney.permissions = [x.noob, x.member];

let cmdMakeLeader = new command("tourney", "!leader", "Transfers leadership to another user.", async (data)=> {
	dio.del(data.messageID, data);

	const giveto = helpers.getUser(data.args[1]);

	if (giveto) {
		try {
			let isLeader = await data.userdata.getProp(data.userID, "factionleader");

			if (!isLeader) {
				dio.say("🕑 You are not a faction leader. :neutral_face:", data);
				return false;
			}

			let nextLeader = await data.userdata.getProp(giveto, "team");

			if (nextLeader.toUpperCase().startsWith(isLeader)) {
				data.userdata.setProp({
					user: data.userID,
					prop: {
						name: "factionleader",
						data: null
					}
				});

				await data.userdata.setProp({
					user: giveto,
					prop: {
						name: "factionleader",
						data: isLeader
					}
				});

				dio.say(`:military_medal: <@${data.userID}> has made <@${giveto}> the team leader.`, data);
			} else {
				dio.say("🕑 You cannot set leader on another faction member!", data);
				return false;
			}
		} catch (e) {
			logger.log(e, "Error");
		}
	} else {
		dio.say("🕑 I need a user to transfer leadership to: `!leader @user`", data);
	}
});

cmdMakeLeader.permissions = [x.noob, x.member];

module.exports.commands = [
	cmdCreate, cmdAdd, cmdDel, 
	cmdStart, cmdEnd, cmdCheckIn, 
	cmdCheckOut, cmdList, cmdAddMatch, 
	cmdUpdateScore, cmdCheckRound, cmdCheckInRemind, 
	cmdBracket, cmdListReplays, cmdDQ, 
	cmdRando, cmdUp, cmdChallonge, cmdListTournies,
	cmdAddTourney, cmdWinTourney, cmdMakeLeader,
	cmdStartTourney, cmdSetT, cmdSetTR
];
